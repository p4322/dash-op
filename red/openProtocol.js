//@ts-check
/*
  Copyright: (c) 2022, D-Tech
  GNU General Public License v3.0+ (see LICENSE or https://www.gnu.org/licenses/gpl-3.0.txt)
*/

const openProtocol = require("dash-node-op");
const base = require("../base.json");
const { EventEmitter } = require("events");

module.exports = function (RED) {
  // <Begin> --- Config ---
  function OpenProtocolConfig(values) {
    EventEmitter.call(this);
    RED.nodes.createNode(this, values);

    let node = this;

    node.controllerIP = values.controllerIP;
    node.controllerPort = Number(values.controllerPort);
    node.keepAlive = values.keepAlive;
    node.timeout = values.timeout;
    node.retries = values.retries;
    node.rawData = values.rawData;
    node.genericMode = values.generic;
    node.forceLinkLayer = values.linkLayer;
    node.disablemidparsing = values.disablemidparsing;

    node.userDisconnect = false;
    node.onClose = false;

    let parsingDisable = {};

    node.disablemidparsing
      .split(/[,; ]+/)
      .map((item) => {
        return Number(item);
      })
      .forEach((elm) => {
        parsingDisable[elm] = true;
      });

    if (node.forceLinkLayer === "false") {
      node.forceLinkLayer = false;
    } else {
      if (node.forceLinkLayer === "true") {
        node.forceLinkLayer = true;
      } else {
        node.forceLinkLayer = undefined;
      }
    }

    let opts = {
      linkLayerActivate: node.forceLinkLayer,
      genericMode: node.genericMode,
      keepAlive: Number(node.keepAlive),
      rawData: node.rawData,
      timeOut: Number(node.timeout),
      retryTimes: Number(node.retries),
      disableMidParsing: parsingDisable,
    };

    node.connectionStatus = false;

    node.increaseEventListener = function increaseEventListener() {
      this.__maxEventListeners = (this.__maxEventListeners || 0) + 1;
      node.log(`increasing maxEventListeners -> ${this.__maxEventListeners}`);
      this.setMaxListeners(this.__maxEventListeners);
    };

    node.connect = function connect() {
      clearTimeout(node.timerReconnect);

      if (node.connectionStatus) {
        node.warn("connection exists");
        node.emit("reuseConnection");
        return;
      }

      if (node.op) {
        node.disconnect();
      }

      node.userDisconnect = false;

      node.op = openProtocol.createClient(
        node.controllerPort,
        node.controllerIP,
        opts,
        (data) => node.onConnect(data)
      );
      node.op.on("error", (err) => node.onErrorOP(err));
    };

    // we do not need auto connect
    //node.connect();

    node.onConnect = function onConnect(data) {
      clearTimeout(node.timerReconnect);

      node.connectionStatus = true;

      node.op.on("__SubscribeData__", (data) => node.onSubscribeDataOP(data));
      node.op.on("close", (error) => node.onCloseOP(error));
      node.op.on("connect", (data) => node.onConnectOP(data));
      node.op.on("data", (data) => node.onDataOP(data));
      node.emit("connect", data);
    };

    node.disconnect = function disconnect() {
      clearTimeout(node.timerReconnect);
      node.removeListenersOP();

      node.op.close();

      node.connectionStatus = false;

      node.userDisconnect = true;

      node.emit("disconnect");
    };

    node.reconnect = function reconnect() {
      clearTimeout(node.timerReconnect);

      if (node.onClose || node.userDisconnect || node.connectionStatus) {
        return;
      }

      node.connect();
    };

    // Begin::Event of Session Control Client - Open Protocol
    node.onErrorOP = function onErrorOP(error) {
      if (node.onClose) {
        return;
      }

      if (
        error.address &&
        error.port &&
        (error.address != node.controllerIP ||
          error.port != node.controllerPort)
      ) {
        node.warn(
          `controller changed: ${error.address}->${node.controllerIP} | ${error.port}->${node.controllerPort}`
        );
        return;
      }

      node.connectionStatus = false;

      node.emit("disconnect");

      node.error(
        `${RED._("open-protocol.message.failed-connect")} ${error.address}:${
          error.port
        } ${error.code}`
      );

      node.removeListenersOP();

      clearTimeout(node.timerReconnect);

      if (
        node.midGroup === "Connect" &&
        node.name.toLowerCase() === "on error"
      ) {
        let message = {
          onError: true,
          error: `${RED._("open-protocol.message.failed-connect")} ${
            error.address
          }:${error.port} ${error.code}`,
        };
        node.send(message);
      }

      node.timerReconnect = setTimeout(() => node.reconnect(), 5000);
    };

    node.onCloseOP = function onCloseOP(error) {
      if (node.onClose) {
        return;
      }

      node.connectionStatus = false;
      node.emit("disconnect");

      node.removeListenersOP();

      clearTimeout(node.timerReconnect);
      node.timerReconnect = setTimeout(() => node.reconnect(), 5000);
    };

    node.onConnectOP = function onConnectOP(data) {
      node.emit("connect", data);
    };

    node.onDataOP = function onDataOP(data) {
      node.emit("data", data);
    };

    node.onSubscribeDataOP = function onSubscribeDataOP(data) {
      node.emit(data.key, data.data);
    };

    node.removeListenersOP = function removeListenersOP() {
      node.op.removeListener("error", (err) => node.onErrorOP(err));
      node.op.removeListener("__SubscribeData__", (data) =>
        node.onSubscribeDataOP(data)
      );
      node.op.removeListener("close", (error) => node.onCloseOP(error));
      node.op.removeListener("connect", (data) => node.onConnectOP(data));
      node.op.removeListener("data", (data) => node.onDataOP(data));
    };
    // End::Event of Session Control Client - Open Protocol

    node.on("close", () => {
      node.onClose = true;
      clearTimeout(node.timerReconnect);

      node.removeListenersOP();

      node.connectionStatus = false;
      node.emit("disconnect");
      node.op.close();
    });
  }

  RED.nodes.registerType("op config", OpenProtocolConfig);
  // <End> --- Config

  // <Begin> --- Node
  function OpenProtocolNode(values) {
    RED.nodes.createNode(this, values);

    let node = this;

    node.config = RED.nodes.getNode(values.config);

    {
      node.config.increaseEventListener();
    }

    node.midGroup = values.midGroup;
    node.customMid = values.customMid;
    node.revision = values.revision;
    node.customRevision = values.customRevision;
    node.autoSubscribe = values.autoSubscribe;
    node.forwardErrors = values.forwardErrors;

    node.config.on("reuseConnection", () => node.onReUseConnection());
    node.config.on("connect", (data) => node.onConnect(data));
    node.config.on("disconnect", () => node.onDisconnect());

    node.onReUseConnection = function onReUseConnection() {
      if (node.midGroup === "Connect") {
        if (node.id === node.config?.___lastConnectionRequestFromNodeId) {
          node.warn("reusing active connection");
          node.send({ status: "already connected" });
        }
      }
    };

    node.onConnect = function onConnect(data) {
      node.status({
        fill: "green",
        shape: "ring",
        text: RED._("open-protocol.util.label.connected"),
      });

      if (node.midGroup === "Connect") {
        if (node.id === node.config?.___lastConnectionRequestFromNodeId) {
          let message = {};
          if (node.config?.___connectionInitializationTime) {
            let endDate = new Date();
            message.timeDifferenceFromConnectionInitializationSeconds =
              (endDate.getTime() -
                node.config?.___connectionInitializationTime.getTime()) /
              1000;
          }
          message.payload = data.payload;
          setMessage(message, data);
          node.send(message);
        }
      }

      if (base[node.midGroup]) {
        if (
          node.autoSubscribe &&
          base[node.midGroup].typeRequest === "SUBSCRIBE"
        ) {
          if (
            node.midGroup == "lastTraceCurve" ||
            node.midGroup == "lastTraceCurveDesoutter"
          ) {
            node.warn("_autoSubscribe detected: " + node.midGroup);
          }
          onInput(node, {});
        }
      }
    };

    node.onDisconnect = function onDisconnect() {
      node.status({
        fill: "red",
        shape: "ring",
        text: RED._("open-protocol.util.label.disconnected"),
      });

      if (base[node.midGroup]) {
        let reference = base[node.midGroup];
        node.config.removeListener(reference.family, node.onSubscribe);
      }

      if (
        node.midGroup === "Connect" &&
        node.config.userDisconnect == false &&
        node.name.toLowerCase() === "on disconnect"
      ) {
        let message = {
          onDisconenct: true,
        };
        //message.payload = data.payload;
        //setMessage(message, data);
        node.send(message);
      }
    };

    node.onData = function onData(data) {
      let message = {};
      message.payload = data.payload;
      setMessage(message, data);

      if (node.midGroup === "Custom") {
        node.send(message);
      } else {
        node.send([null, message]);
      }
    };

    node.onSubscribe = function onSubscribe(data) {
      let message = {};
      message.payload = data.payload;
      setMessage(message, data);

      //Alarm MID  ->> [Feedback, Data, Status, ACK]
      if (node.midGroup === 71) {
        if (data.mid === 71) {
          //Data
          node.send([null, message, null, null]);
          return;
        }

        if (data.mid === 76) {
          //Status
          node.send([null, null, message, null]);
          return;
        }

        if (data.mid === 74) {
          //ACK
          node.send([null, null, null, message]);
          return;
        }
      } else {
        node.send([null, message]);
      }
    };

    if (node.midGroup === "Custom") {
      node.config.on("data", (data) => node.onData(data));
    }

    node.on("input", (msg) => {
      if (node.midGroup === "Custom") {
        let opts = {
          revision: msg.revision,
          payload: msg.payload,
        };

        node.config.op.sendMid(msg.mid, opts).catch((err) => {
          msg.error = err.stack || err;

          if (node.forwardErrors) {
            node.send([msg, null]);
          }

          node.error(
            `${RED._("open-protocol.message.error-send-mid")} - ${err}`,
            msg
          );

          if (
            node.midGroup === "Connect" &&
            node.name.toLowerCase() === "on error"
          ) {
            let message = {
              onMIDError: true,
              error: `${RED._(
                "open-protocol.message.error-send-mid"
              )} - ${err}`,
            };
            node.send(message);
          }
        });

        return;
      }

      if (node.midGroup === "Connect") {
        let disconnectRequired = false;

        switch (typeof msg.controllerIP) {
          case "string":
            if (msg.controllerIP != node.config.controllerIP) {
              node.config.controllerIP = msg.controllerIP;
              disconnectRequired = true;
            }
            break;
          case "undefined":
            break;
          default:
            node.warn(`Invalid controllerIp: ${msg.controllerIP}`);
            break;
        }

        switch (typeof msg.controllerPort) {
          case "number":
            if (msg.controllerPort != node.config.controllerPort) {
              node.config.controllerPort = msg.controllerPort;
              disconnectRequired = true;
            }
            break;
          case "undefined":
            break;
          default:
            node.warn(`Invalid controllerPort: ${msg.controllerPort}`);
            break;
        }

        if (msg.connect != undefined) {
          if (msg.connect) {
            node.config.___lastConnectionRequestFromNodeId = node.id;
            node.config.___connectionInitializationTime = new Date();
            if (node.config.op && disconnectRequired) {
              node.config.disconnect();
              if (
                node.config.connectionStatus == false &&
                node.config.userDisconnect == true
              ) {
                node.config.connect();
              } else {
                node.warn("calling long method");
                setTimeout(() => {
                  node.config.connect();
                }, 500);
              }
            } else {
              node.config.connect();
            }
          } else {
            node.config.userDisconnect = true;
            node.config.disconnect();
          }
        }

        return;
      }

      onInput(node, msg);
    });

    node.on("close", () => {
      node.config.removeListener("connect", (data) => node.onConnect(data));
      node.config.removeListener("disconnect", () => node.onDisconnect());
      node.config.removeListener("reuseConnection", () =>
        node.onReUseConnection()
      );

      if (base[node.midGroup]) {
        let reference = base[node.midGroup];
        node.config.removeListener(reference.family, node.onSubscribe);
      }

      //Alarm MID
      if (node.midGroup === 71) {
        node.config.removeListener("alarmAcknowledged", node.onSubscribe);
        node.config.removeListener("alarmStatus", node.onSubscribe);
      }
    });

    function onInput(node, msg) {
      let reference = base[node.midGroup];
      let opts = {};
      opts.revision = node.revision || msg.revision;
      opts.revision = node.adjustRevision(opts.revision);

      const supportedDefaultMessages = {
        "angle & torque & current & gradient & stroke & force":
          "0                             06001002003004005006",
        "angle & torque & current & gradient & stroke":
          "0                             05001002003004005",
        "angle & torque & current & gradient":
          "0                             04001002003004",
        "angle & torque & current": "0                             03001002003",
        "angle & torque": "0                             02001002",
        angle: "0                             01001",
      };
      if (msg.defaultExtraData) {
        if (
          !Object.values(supportedDefaultMessages).includes(
            msg.defaultExtraData
          )
        ) {
          node.error(`Failed to set default extra data. Unsupported`, msg);
          return;
        }
        node._defaultExtraData = msg.defaultExtraData;
        const selectedKeyByValue = Object.entries(
          supportedDefaultMessages
        ).find(([key, value]) => value === msg.defaultExtraData);
        msg.status = selectedKeyByValue?.[0];
        node.send(msg);
        return;
      }

      if (msg.payload) {
        opts.payload = msg.payload || "";
      }

      if (reference.family === "lastTraceCurve") {
        opts.midNumber = 900;
        // opts.extraData = "0                             03001002003"; // angle & torque & current
        if (node._defaultExtraData) {
          node.log("using default extra data: " + node._defaultExtraData);
        }
        opts.extraData =
          node._defaultExtraData ?? supportedDefaultMessages["angle & torque"]; // angle & torque
        //opts.extraData = "0                             01001"; // angle
        opts.dataLength = opts.extraData.length;
        opts.revision = 1;
        opts.mid = 8;
      }

      if (reference.family === "lastTraceCurveDesoutter") {
        if (node.midGroup == 7408) {
          opts.extraData = "";
          opts.dataLength = opts.extraData.length;
          opts.revision = 1;
          opts.mid = 7408;
        } else {
          console.error(
            "[error] *** lastTraceCurve|Desoutter not implemented for midGroup: " +
              node.midGroup
          );
        }
      }

      switch (reference.typeRequest) {
        case "REQUEST":
          //node.warn(JSON.stringify(reference.family));
          //node.warn(JSON.stringify(opts));
          node.config.op
            .request(reference.family, opts)
            .then((data) => {
              msg.payload = data.payload;
              setMessage(msg, data);
              node.send(msg);
            })
            .catch((err) => {
              msg.error = err.stack || err;
              if (node.forwardErrors) {
                node.send(msg);
              }
              node.error(
                `${RED._("open-protocol.message.error-request")} - ${err}`,
                msg
              );
            });

          break;

        case "SUBSCRIBE":
          if (msg.subscribe !== undefined && !msg.subscribe) {
            node.config.op
              .unsubscribe(reference.family)
              .then((data) => {
                msg.payload = data.payload;
                setMessage(msg, data);
                node.send([msg, null]);
                node.config.removeListener(reference.family, node.onSubscribe);

                //Alarm MID
                if (node.midGroup === 71) {
                  node.config.removeListener(
                    "alarmAcknowledged",
                    node.onSubscribe
                  );
                  node.config.removeListener("alarmStatus", node.onSubscribe);
                }
              })
              .catch((err) => {
                msg.error = err.stack || err;
                if (node.forwardErrors) {
                  node.send([msg, null]);
                }
                node.error(
                  `${RED._(
                    "open-protocol.message.error-unsubscribe"
                  )} - ${err}`,
                  msg
                );
              });

            return;
          }

          node.config.op
            .subscribe(reference.family, opts)
            .then((data) => {
              msg.payload = data.payload;
              setMessage(msg, data);
              node.send([msg, null]);

              node.config.on(reference.family, node.onSubscribe);

              //Alarm MID
              if (node.midGroup === 71) {
                node.config.on("alarmAcknowledged", node.onSubscribe);
                node.config.on("alarmStatus", node.onSubscribe);
              }
            })
            .catch((err) => {
              if (node.midGroup == 8 && reference.family == "lastTraceCurve") {
                node.status({ fill: "red", shape: "ring", text: err });

                msg.error = err;
                node.send([msg, null]);
                return;
              }

              if (
                node.midGroup == 7408 &&
                reference.family == "lastTraceCurveDesoutter"
              ) {
                node.status({ fill: "red", shape: "ring", text: err });

                msg.error = err;
                node.send([msg, null]);
                return;
              }

              msg.error = err.stack || err;

              if (node.forwardErrors) {
                node.send([msg, null]);
              }

              node.error(
                `${RED._("open-protocol.message.error-subscribe")} - ${err}`,
                msg
              );
              node.warn("reconnecting...");
              node.config.disconnect();
              node.config.connect();
            });

          break;

        case "COMMAND":
          node.config.op
            .command(reference.family, opts)
            .then((data) => {
              msg.payload = data.payload;
              setMessage(msg, data);
              node.send(msg);
            })
            .catch((err) => {
              msg.error = err.stack || err;
              if (node.forwardErrors) {
                node.send(msg);
              }
              node.error(
                `${RED._("open-protocol.message.error-command")} - ${err}`,
                msg
              );

              if (
                node.midGroup === "Connect" &&
                node.name.toLowerCase() === "on error"
              ) {
                let message = {
                  onCommandError: true,
                  error: `${RED._(
                    "open-protocol.message.error-command"
                  )} - ${err}`,
                };
                node.send(message);
              }
            });

          break;
      }
    }

    node.adjustRevision = function adjustRevision(revision) {
      if (revision === "Custom") {
        return node.customRevision;
      }

      if (revision === "Auto") {
        return undefined;
      }

      return Number(revision);
    };

    function setMessage(msg, data) {
      msg.mid = data.mid;
      msg.revision = data.revision;
      msg.noAck = data.noAck;
      msg.stationID = data.stationID;
      msg.spindleID = data.spindleID;
      msg.sequenceNumber = data.sequenceNumber;
      msg.messageParts = data.messageParts;
      msg.messageNumber = data.messageNumber;

      if (node.config.rawData) {
        msg._rawData = data._raw;
      }
    }
  }

  RED.nodes.registerType("op node", OpenProtocolNode);
  // <End> --- Node
};
